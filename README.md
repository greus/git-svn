# git-svn

https://git-scm.com/docs/git-svn

## Init "single branch" style

    git svn clone {path/to/trunk} {dest}

or

    git svn init {path/to/trunk} {dest}
    cd {dest}
    git svn fetch
    git rebase git-svn

## Fetch new revisions

    git svn fetch
    git rebase git-svn

or

    git svn rebase

## Git config

    git config user.name {svn author}

## Svn ignore

    git svn show-ignore >> .git/info/exclude

## Commit into svn
Creates one svn commit for each git commit

    git svn dcommit
